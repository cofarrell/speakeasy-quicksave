/**
 * The main module
 *
 * @context editor
 */
var $ = require('speakeasy/jquery').jQuery;

AJS.bind("init.rte", function() {
    var publish = $('form.editor #rte-button-publish');
    if (publish.length == 0) return;

    $('.save-button-container').prepend('<button class="aui-button aui-button-primary" title="Save ( Type \'⌘+S\' )" id="rte-button-save" value="Draft" name="save">Save</button>');
    var cancelButton = $('#rte-button-cancel');
    cancelButton.hide();
    cancelButton.after('<button class="toolbar-item-link aui-button aui-button-link" id="rte-button-close" value="Cancel" name="cancel"><span class="trigger-text">Close</span></button>');
    var saveButton = $('#rte-button-save');
    var save = function(options) {
        options = options || {};
        options.forceSave = true;
        saveButton.disable();
        Confluence.Editor.Drafts.save(options);
    }
    saveButton.click(function(e){ 
        save();
        e.preventDefault();
    });
    publish.text('Publish');
    var waitWhile = function(cond, after, count) {
        setTimeout(function() {
            count = count || 0;
            if (count > 100) {
                return;
            }
            if(cond()) {
                waitWhile(cond, after, count + 1);
                return;
            }
            after();
        }, 1000);
    };
    $.each(tinymce.editors, function(i, t) {
        // TODO Yuck - is there a way to this after everything else?
        waitWhile(function() {
            return !t.shortcuts['ctrl,,,83'];
        }, function() {
            t.addShortcut('ctrl+s', t.getLang('save.save_desc'), 'mceSaveDraft');
            t.addCommand('mceSaveDraft', save);
        });
    })
    $('#rte-button-close').click(function(e) {
        var doClose = function() {
            cancelButton.attr('value', 'Save');
            cancelButton.click();
        }
        if (!Confluence.Editor.hasContentChanged())
        {
            return doClose();
        }

        var dialog = new AJS.Dialog({width:400, height:250, id:"draft-confirmation-dialog", closeOnOutsideClick: true});

        dialog.addHeader("Draft Confirmation");

        var i1 = '<input class="radio" type="radio" checked="checked" name="save-to-drafts" id="save-to-drafts" ><label>Save to my drafts</label>'
        var i2 = '<input class="radio" type="radio" name="save-to-drafts" ><label>Discard changes</label>'
        var p = function(s) { return "<p>" + s + "</p>"}
        dialog.addPanel("Panel 1", "<p>Changes you have made have not been saved. What would you like to do?<p>" + p(i1) + p(i2), "panel-body");

        dialog.addButton("OK", function (dialog) {
            if ($('#save-to-drafts').attr('checked')) {
                save({
                    onSuccessHandler: doClose
                });
            } else {
                doClose();
            }
        });
        dialog.addButton("Cancel", function (dialog) {
            dialog.hide();
        });
        dialog.show();
        // TODO hack
        $('#save-to-drafts').attr('checked', 'true');
        e.preventDefault();
    });
    $.each(tinymce.editors, function(i, ed) {
        ed.onKeyDown.add(function(){
            saveButton.enable();
        });
    });
});